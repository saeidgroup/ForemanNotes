# Foreman Notes

This is the repo for notes on Foreman installation and maintenance.

## What is Foreman?

Foreman allows us to define many objects, which serve different purposes. 
But the heart of everything is the Host object, it glues all other objects together. 
A host represents a machine. It can be a device on the network or simply something, 
that you want to install an operating system on. It’s typically a thing you want 
to manage or automate some tasks around it. In virtualization terminology, 
it can be hypervisor but also a guest. 
It can be a baremetal machine, it can be VM running on some hypervisor or a VM somewhere in the cloud. 
While Foreman does not primarily target these, it can also be a router, IoT sensor, laptop, container.

## Installation 

In the following we assume we are in a fresh AlamLinux 8 installation on localhost and 
proceed with installng Katello.

**Note**: Installing Katello in a pre-installed Foreman environment can result in unpredicted behavior.

### Manual installation
```bash
# update packages
sudo dnf update -y
# enable epel repository
sudo dnf install -y epel-release
# enable powertools
sudo crb enable
# enable foreman repository
sudo dnf -y install https://yum.theforeman.org/releases/nightly/el8/x86_64/foreman-release.rpm
# enable puppet's 7.x repository
sudo dnf install -y https://yum.puppet.com/puppet7-release-el-8.noarch.rpm
# install katello 
dnf install -y https://yum.theforeman.org/katello/nightly/katello/el8/x86_64/katello-repos-latest.rpm
# enable katello dnf module
sudo dnf enable -y module katello:el8
# install the installer
sudo dnf install -y foreman-installer-katello
```
now launch the installer, grab a coffee and cross your fingers:
```bash
sudo foreman-installer --scenario katello --tuning development -l DEBUG
```
**Note**: There are many flags that enable various plugins and modules such as ansible, 
these can also be provided later on with another installer launch. For various switches, 
see: [foreman installer options](https://theforeman.org/manuals/2.0/index.html#3.2.2InstallerOptions).

## Working with Foreman

## Notes
- As of writing this note installing Foreman with Katello is only supported on EL8 and a stable release still 
lacks for EL9. Moreover Katello is not supported on Debian, and Foreman itself still lacks stable support for 
bookworm (main reason being that Foreman depends on ruby 2.7 but bookworm ships with ruby 3.0 and installing ruby 2.7 
on a production Debian 12 server is ill advised since it requires changing in fundamental packages such as OpenSSL.)

## Relevant resources

1. [forklifting foreman](https://github.com/theforeman/forklift)
2. [starting with foreman](https://theforeman.org/2020/12/how-to-start-with-foreman.html)
